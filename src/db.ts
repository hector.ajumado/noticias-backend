import { DataSource } from "typeorm";
import { News } from "./entities/news.entity";

export const AppDataSource = new DataSource({
    type: 'postgres',
    host: 'localhost',
    username: 'postgres',
    password: '9480',
    port: 5432,
    database: 'el_hocicon',
    entities: [News],
    logging: true,
    synchronize: true
})