import { Request, Response } from "express";
import { News } from "../entities/news.entity";

export const createNews = async (req: Request, res: Response) => {
  try {
    const { title, location, author, content } = req.body

    const news =  new News();
    news.title = title;
    news.location = location;
    news.author = author;
    news.content = content;

    await news.save();

    return res.json(news);
  } catch (error) {
    if(error instanceof Error) {
      return res.status(500).json({message: error.message});
    }
  }
}

export const getNews = async (req: Request, res: Response) => {
  try {
      const news = await News.find();
      return res.json(news);
  } catch (error) {
      if(error instanceof Error) {
          return res.status(500).json({message: error.message});
      }
  }
  
}

export const updateNews = async(req: Request, res: Response) => {
  try {
      const {id} = req.params

      const news = await News.findOneBy({id: parseInt(req.params.id)});

      if(!news) return res.status(404).json({message: 'News does not ixists'});

      await News.update({id: parseInt(id)}, req.body);

      return res.sendStatus(204);
  } catch (error) {
      if(error instanceof Error) {
          return res.status(500).json({message: error.message});
      }
  }
}

export const deleteNews = async (req: Request, res: Response) => {
  try {
      const {id} =req.params;
      const result = News.delete({id: parseInt(id)});

      if((await result).affected === 0) {
          return res.status(404).json({message: 'News not found'});
      }
      return res.status(204);
  } catch (error) {
      if(error instanceof Error) {
          return res.status(500).json({message: error.message});
      }
  }
}

export const getOneNews = async (req: Request, res: Response) => {
  try {
      const {id} = req.params;
      const news = await News.findOneBy({id: parseInt(id)});

      return res.json(news);
  } catch (error) {
      if(error instanceof Error) {
          return res.status(500).json({message: error.message});
      }
  }
}