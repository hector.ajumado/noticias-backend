import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, CreateDateColumn } from 'typeorm';

@Entity()
export class News extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column({ nullable: true })
  imageUrl: string;

  @CreateDateColumn()
  publicationDate: Date;

  @Column()
  location: string;

  @Column()
  author: string;

  @Column('text')
  content: string;
}
