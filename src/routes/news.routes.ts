import { Router } from "express";
import { createNews, deleteNews, getNews, getOneNews, updateNews } from "../controllers/news.controller";

const router = Router();

router.post('/news', createNews);

router.get('/news', getNews);

router.put('/news/:id', updateNews);

router.delete('/news/:id', deleteNews);

router.get('/news/:id', getOneNews);

export default router;