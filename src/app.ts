import express from 'express';
import morgan from 'morgan';
import cors from 'cors';

import newsRoutes from "./routes/news.routes";

const app = express();

app.use(morgan('dev'));
app.use(cors());

app.use(express.json())

app.use(newsRoutes);

export default app;